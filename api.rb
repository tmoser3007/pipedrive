require 'rubygems'
require 'bundler/setup'

require 'rest_client'
require 'json'

def url_for(url, params = {})
  base = "https://api.pipedrive.com/v1/#{url}?api_token=305c80eac719ff3e1b4e615b448b32e4ffce5ea5"
  params.each do |k,v|
    base += "&#{k}=#{v}"
  end
  return base
end

def get(url, params = {})
  response = RestClient.get url_for(url, params)
  JSON.parse(response)
end

more_items = true
deals = []
start = 0
while more_items
  result = get('deals', limit: 500, start: start)
  more_items = result['additional_data']['pagination']['more_items_in_collection']
  deals << result['data']
  start += 500
end

deals.flatten!(1)

File.open("data/#{Time.now.to_i}.json","w") do |f|
  f.write(deals.to_json)
end

