require 'rubygems'
require 'bundler/setup'

require 'json'
require 'date'


def find_in_array_id(ary, id)
  ary.select{|e| e[:id] == id}.first
end


puts filenames = Dir["data/*.json"]

labels = []

filenames.each do |filename|
  timestamp = filename[5...-5].to_i
  label = Time.at(timestamp).strftime("%d-%m-%y")
  labels << label
end

loaded_datasets = {}

filenames.each do |filename|
  json = JSON.parse(File.read("#{filename}"))
  filtered = json.map do |deal|
    
    won = nil
    won = false if !deal['lost_time'].nil?
    won = true if !deal['won_time'].nil?

    if deal['stage_id'] == 6
      deal['stage_id'] = "Idea"
    elsif deal['stage_id'] == 8
      deal['stage_id'] = "Identified"
    elsif deal['stage_id'] == 9
      deal['stage_id'] = "Proposal"
    elsif deal['stage_id'] == 10
      deal['stage_id'] = "Short Listed"
    elsif deal['stage_id'] == 11
      deal['stage_id'] = "Verbal"
    elsif deal['stage_id'] == 12
      deal['stage_id'] = "Started"
    elsif deal['stage_id'] == 13
      deal['stage_id'] = "Signed"
    elsif deal['stage_id'] == 14
      deal['stage_id'] = "Ended"
    else
      deal['stage_id'] = "Not defined"
    end
    
    {
      id: deal['id'],
      title: deal['title'],
      stage_id: deal['stage_id'],
      won: won,
      value: deal['value'].to_i,
      active: deal['active'],
      pipeline_id: deal['pipeline_id'],
      owner_name: deal['owner_name'],
      update_time: deal['update_time'],
      currency: deal['currency'],
      probability: deal['9c01899b488f7b158aa3d652b1dab109fd6cca91']
    }

  end
  loaded_datasets[filename] = filtered
end

results = []

loaded_datasets[filenames[-2]].each do |a|
  if (a[:active] == true)
    b = find_in_array_id(loaded_datasets[filenames[-1]], a[:id])
    unless b.nil?

      attrs_to_check = [:title, :stage_id, :won, :value, :active, :pipeline_id, :owner_name]
      changed = attrs_to_check.map{|e| a[e] != b[e]}.reduce{|mem, n| mem || n}
      if changed        
        result = {
          title: a[:title],
          value_changed: (b[:value] - a[:value]),
          stage_id_original: a[:stage_id],
          stage_id_new: b[:stage_id],
          success_level_original: a[:won],
          success_level_new: b[:won],
          active_original: a[:active],
          active_new: b[:active],
          owner_name: b[:owner_name],
          pipeline_id: a[:pipeline_id],
          probability_original: a[:probability],
          probability_new: b[:probability]
        }

        results << result
      end
    end
  end
end


results_new_deals = []

loaded_datasets[filenames[-1]].each do |b|
  a = find_in_array_id(loaded_datasets[filenames[-2]], b[:id])
  if a == nil
    result_new_deal = {
      title: b[:title],
      value: b[:value].to_s.reverse.gsub(/...(?=.)/,'\&.').reverse,
      owner_name: b[:owner_name],
      stage_id_new: b[:stage_id],
      pipeline_id: b[:pipeline_id]
    }
    results_new_deals << result_new_deal
  end
end


rotten = []

loaded_datasets[filenames[-1]].each do |b|
  if Time.parse(b[:update_time]) <= Time.now - (60 * 60 * 24 * 7 * 2)
    data = {
      title: b[:title],
      owner_name: b[:owner_name],
      stage_id_new: b[:stage_id],
      pipeline_id: b[:pipeline_id],
      update_time: b[:update_time]
    }
    rotten << data
  end
end


chart = {}
chart_1 = {}
chf = []
eur = []

loaded_datasets.each do |key, dataset|
  eur = dataset.find_all{|deal| deal[:stage_id] == "Short Listed" && deal[:active] == true && deal[:currency] == "EUR"}
    eur_sum = eur.map { |deal| deal[:value] || 0 }.reduce(:+)
  chf = dataset.find_all{|deal| deal[:stage_id] == "Short Listed" && deal[:active] == true && deal[:currency] == "CHF"}
    chf_sum = chf.map { |deal| (deal[:value]*0.83) || 0 }.reduce(:+)
  if chf_sum.nil?
    chart[key] = eur_sum
  else
    chart[key] = eur_sum + chf_sum
  end
end


high = {}

loaded_datasets.each do |key, dataset|
  prob_high = dataset.find_all{|deal| deal[:probability] == "41" && deal[:active] == true}
    high[key] = prob_high.map { |deal| deal[:value] || 0 }.reduce(:+)
end


middle = {}

loaded_datasets.each do |key, dataset|
  prob_middle = dataset.find_all{|deal| deal[:probability] == "42" && deal[:active] == true}
    middle[key] = prob_middle.map { |deal| deal[:value] || 0 }.reduce(:+)
end


low = {}

loaded_datasets.each do |key, dataset|
  prob_low = dataset.find_all{|deal| deal[:probability] == "43" && deal[:active] == true}
    low[key] = prob_low.map { |deal| deal[:value] || 0 }.reduce(:+)
end


owner_estimation_accuracy = []

latest_loaded_dataset = loaded_datasets[filenames[-1]]
latest_loaded_dataset_owners = latest_loaded_dataset.map { |ds| ds[:owner_name] }.uniq

latest_loaded_dataset_owners.each do |owner_name|
  won_deals_of_owner = latest_loaded_dataset.find_all{|deal| deal[:owner_name] == owner_name && deal[:won] != false && %w(Verbal Signed Started).include?(deal[:stage_id])}
  lost_deals_of_owner = latest_loaded_dataset.find_all{|deal| deal[:owner_name] == owner_name && deal[:won] == false}
  
  incorrect_count = won_deals_of_owner.find_all { |deal| deal[:probability] == "43"}.size
  correct_count = won_deals_of_owner.find_all { |deal| deal[:probability] == "41" || deal[:probability] == "42"}.size

  incorrect_count += lost_deals_of_owner.find_all { |deal| deal[:probability] == "41" || deal[:probability] == "42"}.size
  correct_count += lost_deals_of_owner.find_all { |deal| deal[:probability] == "43"}.size

  owner_estimation_accuracy << {owner_name: owner_name, incorrect_count: incorrect_count, correct_count: correct_count} if (incorrect_count + correct_count) > 0 
end


template = File.read("template.html")
template.sub! '___RESULTS___', JSON.pretty_generate(results)
template.sub! '___RESULTS_NEW_DEALS___', JSON.pretty_generate(results_new_deals)
template.sub! '___RESULTS_ROTTEN___', JSON.pretty_generate(rotten)
template.sub! '___RESULTS_VALUES___', JSON.pretty_generate(chart)
template.sub! '___RESULTS_FILENAMES___', JSON.pretty_generate(labels)
template.sub! '___RESULTS_HIGH_PROBABILITY___', JSON.pretty_generate(high)
template.sub! '___RESULTS_MIDDLE_PROBABILITY___', JSON.pretty_generate(middle)
template.sub! '___RESULTS_LOW_PROBABILITY___', JSON.pretty_generate(low)
template.sub! '___RESULTS_ESTIMATION_ACCURACY___', JSON.pretty_generate(owner_estimation_accuracy)


File.open('report/index.html', 'w') do |f|
  f.puts template
end
